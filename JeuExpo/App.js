import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import BestGameEver from './Components/Play/Game';
import Menu from './Components/Menu';
import Score from './Components/Score';
import {NavigationContainer} from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
          <Stack.Navigator
              screenOptions={{
                  gestureEnabled: false,
                  headerStyle: {
                      backgroundColor: '#004000'
                  },
                  headerTitleStyle: {
                      fontWeight: 'bold',
                  },
                  headerTintColor: 'white',
                  headerBackTitleVisible: false
              }}>
              <Stack.Screen
                  name="MenuScreen"
                  options={{ title: 'Menu' }}
                  component={Menu}
              />
              <Stack.Screen
                  name="ScoreScreen"
                  options={{ title: 'Scores' }}
                  component={Score}
              />
              <Stack.Screen
                  name="BestGameEver"
                  options={{ title: 'Super Golf' }}
                  component={BestGameEver}
              />
          </Stack.Navigator>
      </NavigationContainer>
  );
};
