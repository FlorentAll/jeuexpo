// Navigation/Navigation.js

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Menu from '../Components/Menu'
import Game from '../Components/Play/Game'

const GameStackNavigator = createStackNavigator({
    Menu: {
        screen: Menu,
        navigationOptions: {
            title: 'Menu',
        }
    },
    Game: {
        screen: Game,
    }
});

export default createAppContainer(GameStackNavigator)