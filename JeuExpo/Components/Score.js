import React, {Component} from 'react';
import {StyleSheet, View, WebView, Text, ActivityIndicator, TouchableOpacity, Button, ImageBackground} from 'react-native';

export default class Score extends Component {
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>SCORES</Text>
                <TouchableOpacity
                    style={styles.buttonPlay}
                    onPress={() => navigate('BestGameEver')}>
                    <Text style={styles.text}>START</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'green',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonPlay: {
        height: 150,
        width: 150,
        backgroundColor:'red',
        color:'orange',
        borderColor: 'black',
        borderRadius: 75,
        borderWidth: 2,
    },
    title:{
        fontSize: 70,
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        backgroundColor: '#004000',
        paddingLeft: 25,
        paddingRight: 25,
        marginBottom: 10,
        borderColor: 'black',
        borderWidth: 2,
        textAlignVertical: 'top'
    },
    text: {
        flex:1,
        color: 'white',
        textAlign: 'center',
        fontSize:40,
        fontWeight: 'bold',
        textAlignVertical: 'center'
    }
})