import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, TouchableOpacity, Alert, Dimensions } from 'react-native';
import Matter from "matter-js";
import {GameEngine} from "react-native-game-engine";
import Ball from './Ball';
import Hole from './Hole';
import Wall from './Wall';
import Physics from './Physics';
import {Constants} from './Constants';
import {Vibration} from 'react-native';
//import {AsyncStorage} from 'react-native';

const width = Dimensions.get("screen").width;
const height = Dimensions.get("screen").height;

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            running: true,
            field: 1,
            life: 20,
            move: false,
            changeField: false
        };

        this.gameEngine = null;

        this.entities = this.setupWorld();
    }

    setupWorld = () => {
        let engine = Matter.Engine.create({enableSleeping: false});
        let world = engine.world;
        world.gravity.y = 0;
        
        let ball = Matter.Bodies.circle(Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT - 250, 10, {
            label: 'ball',
            restitution: 1.0,
            frictionAir: 0.016
        });
        let hole = Matter.Bodies.circle(100, 100, 4, {
            label: 'hole',
            isStatic: true
        });
        
        //WALL
        let floor = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT - 60, Constants.MAX_WIDTH, 50, { isStatic: true } );
        let wall1 = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, -20, Constants.MAX_WIDTH, 50, { isStatic: true } );
        let wall2 = Matter.Bodies.rectangle( -20, 0, 50, Constants.MAX_HEIGHT * 2, { isStatic: true } );
        let wall3 = Matter.Bodies.rectangle( Constants.MAX_WIDTH + 20, 0, 50, Constants.MAX_HEIGHT * 2, { isStatic: true } );
        
        //OBSTACLES
        let f1wall1 = Matter.Bodies.rectangle( 0, 200, 500, 25, { isStatic: true } );
        let f1wall2 = Matter.Bodies.rectangle( Constants.MAX_WIDTH, 300, 500, 25, { isStatic: true } );
        let f2wall1 = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, 200, 100, 100, { isStatic: true } );
        //Matter.Body.rotate(f2wall1, 45);
        let f3wall1 = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT / 2.5, 200, 25, { isStatic: true } );
        let f4wall1 = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT / 2, 25, Constants.MAX_HEIGHT, { isStatic: true } );
        let f4portal1 = Matter.Bodies.rectangle( Constants.MAX_WIDTH * 0.75, 50, 30, 12, { isStatic: true, label: 'portal1' } );
        let f4portal2 = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 4, (Constants.MAX_HEIGHT / 1.5) + 50, 30, 12, { isStatic: true, label: 'portal2' } );
        
        Matter.World.add(world, [ball, floor, wall1, wall2, wall3, hole, f1wall1, f1wall2]);
    
        const allEntities =  {
            physics: {engine: engine, world: world},
            floor: {body: floor, size: [Constants.MAX_WIDTH, 50], color: "#666", renderer: Wall},
            wall1: {body: wall1, size: [Constants.MAX_WIDTH, 50], color: "#666", renderer: Wall},
            wall2: {body: wall2, size: [50, Constants.MAX_HEIGHT * 2], color: "#666", renderer: Wall},
            wall3: {body: wall3, size: [50, Constants.MAX_HEIGHT * 2], color: "#666", renderer: Wall},
            ball: {body: ball, size: [20, 20], color: "#ff0000", renderer: Ball},
            hole: {body: hole, size: [4, 4], color: "#000000", renderer: Hole},
            f1wall1: {body: f1wall1, size: [500, 25], color: "#888", renderer: Wall},
            f1wall2: {body: f1wall2, size: [500, 25], color: "#888", renderer: Wall},
            f2wall1: {body: f2wall1, size: [100, 100], color: null, renderer: Wall},
            f3wall1: {body: f3wall1, size: [200, 25], color: null, renderer: Wall},
            f4wall1: {body: f4wall1, size: [25, Constants.MAX_HEIGHT], color: null, renderer: Wall},
            f4portal1: {body: f4portal1, size: [30, 12], color: null, renderer: Wall},
            f4portal2: {body: f4portal2, size: [30, 12], color: null, renderer: Wall},
        };
        
        Matter.Events.on(engine, 'collisionStart', (event) => {
            
            var pairs = event.pairs;
            Vibration.vibrate(200);
            if((pairs[0].bodyA.label === "ball" && pairs[0].bodyB.label === "hole") || (pairs[0].bodyA.label === "hole" && pairs[0].bodyB.label === "ball")){
                this.gameEngine.dispatch({ type: "trou"});
            }
            if((pairs[0].bodyA.label === "ball" && pairs[0].bodyB.label === "portal1") || (pairs[0].bodyA.label === "portal1" && pairs[0].bodyB.label === "ball")){
                Matter.Body.setPosition(allEntities.ball.body, {
                    x: (Constants.MAX_WIDTH / 4) - 10,
                    y: (Constants.MAX_HEIGHT / 1.5) + 35
                });
            }
            /*if((pairs[0].bodyA.label === "ball" && pairs[0].bodyB.label === "portal2") || (pairs[0].bodyA.label === "portal2" && pairs[0].bodyB.label === "ball")){
                Matter.Body.setPosition(allEntities.ball.body, {
                    x: Constants.MAX_WIDTH * 0.75,
                    y: 75
                });
            }*/
        });
        
        Matter.Events.on(engine, 'afterUpdate', (event) => {
            
            if(this.state.changeField === true){
                Matter.Body.setPosition(allEntities.ball.body, {
                    x: Constants.MAX_WIDTH / 2,
                    y: Constants.MAX_HEIGHT - 250
                });
                Matter.Body.setVelocity(allEntities.ball.body, {
                    x: 0,
                    y: 0
                });
                if(this.state.field === 2) {
                    allEntities.f1wall1.color = null;
                    allEntities.f1wall2.color = null;
                    Matter.World.remove(world, [f1wall1, f1wall2]);
                    Matter.World.add(world, f2wall1);
                    allEntities.f2wall1.color = 'yellow';
                    Matter.Body.setPosition(allEntities.hole.body, {
                        x: Constants.MAX_WIDTH / 2,
                        y: 100
                    });
                }else if(this.state.field === 3){
                    allEntities.f2wall1.color = null;
                    Matter.World.remove(world, f2wall1);
                    Matter.World.add(world, f3wall1);
                    allEntities.f3wall1.color = 'yellow';
                }else if(this.state.field === 4){
                    allEntities.f3wall1.color = null;
                    Matter.World.add(world, [f4wall1, f4portal1, f4portal2]);
                    allEntities.f4wall1.color = 'yellow';
                    allEntities.f4portal1.color = 'black';
                    allEntities.f4portal2.color = 'black';
                    Matter.World.remove(world, f3wall1);
                    Matter.Body.setPosition(allEntities.hole.body, {
                        x: Constants.MAX_WIDTH / 4,
                        y: 100
                    });
                    Matter.Body.setPosition(allEntities.ball.body, {
                        x: Constants.MAX_WIDTH * 0.75,
                        y: Constants.MAX_HEIGHT / 1.5
                    });
                }
                this.setState({
                    changeField: false
                });
            }
            
            if (Math.abs(allEntities.ball.body.velocity.x) < 0.1 && Math.abs(allEntities.ball.body.velocity.y) < 0.1){
                if(this.state.move === false){
                    allEntities.ball.color = 'white';
                    console.log(allEntities.ball);
                    console.log('- LIFE -1 -');
                    this.setState({
                        life: this.state.life - 1,
                        move: true
                    });
                }
            }else{
                if(this.state.move === true){
                    allEntities.ball.color = 'red';
                }
                this.setState({
                    move: false
                });
            }
            if( this.state.life <= 0){
                this.gameEngine.dispatch({ type: "gameover"});
            }
        });
    
        return allEntities;
    }

    onEvent = (e) => {
        if (e.type === "trou") {
            Vibration.vibrate(500);
            this.setState({
                running: false,
            });
        }
        if (e.type === "gameover") {
            this.setState({
                running: false,
            });
        }
    }

    next = () => {
        Vibration.vibrate(200);
        
        this.setState({
            running: true,
            field: this.state.field + 1,
            life: this.state.life + 1,
            move: false,
            changeField: true
        });
    }
    
    setData = () => {
        Vibration.vibrate(200);
        //AsyncStorage.setItem('test');
        
        this.gameEngine.swap(this.setupWorld());
        this.setState({
            running: true,
            field: 1,
            life: 20,
            move: false,
        });
    }
    
    home = () => {
        Vibration.vibrate(200);
        this.props.navigation.navigate('MenuScreen');
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.score}> Field: {this.state.field}/4</Text>
                <Text style={styles.score}> Life: {this.state.life}</Text>
                <GameEngine
                    ref={(ref) => {
                        this.gameEngine = ref;
                    }}
                    style={styles.gameContainer}
                    systems={[Physics]}
                    running={this.state.running}
                    onEvent={this.onEvent}
                    entities={this.entities}>
                    <StatusBar hidden={false}/>
                </GameEngine>
                {!this.state.running && this.state.life > 0 && this.state.field < 4 && <TouchableOpacity style={styles.fullScreenButton} onPress={this.next}>
                    <View style={styles.fullScreen}>
                        <Text style={styles.gameOverText}>Next Field !</Text>
                        <Text style={styles.score}>Life + 1</Text>
                    </View>
                </TouchableOpacity>}
                {!this.state.running && this.state.life > 0 && this.state.field >= 4 && <TouchableOpacity style={styles.fullScreenButton} onPress={this.home}>
                    <View style={styles.fullScreen}>
                        <Text style={styles.gameOverText}>VICTORY !</Text>
                        <Text style={styles.score}>Score : {this.state.life}</Text>
                    </View>
                </TouchableOpacity>}
                {!this.state.running && this.state.life <= 0 && <TouchableOpacity style={styles.fullScreenButton} onPress={this.setData}>
                    <View style={styles.fullScreen}>
                        <Text style={styles.gameOverText}>GAME OVER</Text>
                        <Text style={styles.score}>Terrain {this.state.field}</Text>
                    </View>
                </TouchableOpacity>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'green',
    },
    gameContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    gameOverText: {
        color: 'white',
        fontSize: 48
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'black',
        opacity: 0.7,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fullScreenButton: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1
    },
    score: {
        color: 'white',
        fontSize: 24
    }
});
