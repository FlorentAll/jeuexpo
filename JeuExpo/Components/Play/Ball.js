
import React, { Component } from "react";
import { View } from "react-native";

export default class Ball extends Component {
    render() {
        const width = this.props.size[0];
        const height = this.props.size[1];
        const x = this.props.body.position.x - width / 2;
        const y = this.props.body.position.y - height / 2;

        return (
            <View
                style={{
                    position: "absolute",
                    borderWidth: 0.5,
                    borderColor: this.props.color,
                    left: x,
                    top: y,
                    borderRadius: 50,
                    width: width,
                    height: height,
                    backgroundColor: 'red',
                    zIndex: 2,
                }} />
    );
  }
}