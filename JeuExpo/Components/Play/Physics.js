import Matter from "matter-js";
import {Constants} from './Constants';
import {Alert} from "react-native";
//import App from "./../../App";

const Physics = (entities, {touches, time}) => {
    let engine = entities.physics.engine;
    let ball = entities.ball;

    touches.filter(t => t.type === "end").forEach(t => {

        if (Math.abs(ball.body.velocity.x) < 0.1 && Math.abs(ball.body.velocity.y) < 0.1) {
            
            let x = t.event.locationX;
            let y = t.event.locationY;
            let x2 = ball.body.position.x;
            let y2 = ball.body.position.y;
            let vx = Math.round((x2 - x) / 10);
            let vy = Math.round((y2 - y) / 10);

            if (vx > 10) {
                vx = 10;
            }
            if (vx < -10) {
                vx = -10;
            }
            if (vy > 10) {
                vy = 10;
            }
            if (vy < -10) {
                vy = -10;
            }
            Matter.Body.setVelocity(ball.body, {
                x: vx,
                y: vy,
            });

        }
    });

    Matter.Engine.update(engine, time.delta);

    return entities;
};

export default Physics;