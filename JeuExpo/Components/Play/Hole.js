
import React, { Component } from "react";
import { View } from "react-native";

export default class Hole extends Component {
    render() {
        const width = this.props.size[0];
        const height = this.props.size[1];
        const x = this.props.body.position.x - width / 2;
        const y = this.props.body.position.y - height / 2;

        return (
            <View
                style={{
                    position: "absolute",
                    left: x - 10,
                    top: y - 10,
                    borderRadius: 50,
                    width: width * 6,
                    height: height * 6,
                    backgroundColor: this.props.color
                }} />
    );
  }
}



