import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from '@react-navigation/stack';

export default class MenuButton extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.touch}>
                <Text style={styles.text}>Play Button</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    touch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        backgroundColor: 'blue',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        size: 40
    }
})